package org.example.wordl.common

import org.junit.jupiter.api.Test
import kotlin.test.assertEquals
import kotlin.test.assertTrue

internal class WordlKtTest {

    @Test
    fun exactMatch() {
        val compareResult = compareWords("abcde", "abcde")
        assertTrue(compareResult.values.all { it.resultType == ResultType.CORRECT })
    }

    @Test
    fun exactMatchCaseInsensitive() {
        val compareResult = compareWords("abcde", "ABCDE")
        assertTrue(compareResult.values.all { it.resultType == ResultType.CORRECT })
    }

    @Test
    fun allMissing() {
        val compareResult = compareWords("abcde", "zyxwv")
        assertTrue(compareResult.values.all { it.resultType == ResultType.MISSING })
    }

    @Test
    fun allContains() {
        val compareResult = compareWords("abcde", "deabc")
        assertTrue(compareResult.values.all { it.resultType == ResultType.PRESENT })
    }

    @Test
    fun allContainsCaseInsensitive() {
        val compareResult = compareWords("abcde", "DEABC")
        assertTrue(compareResult.values.all { it.resultType == ResultType.PRESENT })
    }

    @Test
    fun exactMatchAndContain() {
        val compareResult = compareWords("abcda", "aabcd")

        assertEquals(ResultType.CORRECT, compareResult[0]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[1]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[2]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[3]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[4]!!.resultType)
    }

    @Test
    fun containsOnlyTwice() {
        val compareResult = compareWords("aacde", "ffaaa")

        assertEquals(ResultType.MISSING, compareResult[0]!!.resultType)
        assertEquals(ResultType.MISSING, compareResult[1]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[2]!!.resultType)
        assertEquals(ResultType.PRESENT, compareResult[3]!!.resultType)
        assertEquals(ResultType.MISSING, compareResult[4]!!.resultType)
    }
}
