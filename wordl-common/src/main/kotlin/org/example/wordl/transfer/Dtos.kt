package org.example.wordl.transfer

import kotlinx.serialization.Serializable
import org.example.wordl.common.CharResult

@Serializable
data class ComparisonRequest(
    val username: String,
    val guess: String,
)

@Serializable
data class ComparisonResult(
    val username: String,
    val currentTry: Int,
    val results: Map<Int, CharResult>,
)

@Serializable
data class GameError(
    val username: String,
    val message: String,
)
