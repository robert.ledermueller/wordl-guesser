package org.example.wordl.common

import kotlinx.serialization.Serializable

fun compareWords(solution: String, guess: String): Map<Int, CharResult> {

    // 1. Check if there is an exact match for each char
    val solutionLower = solution.lowercase()
    val guessLower = guess.lowercase()
    val resultMap = mutableMapOf<Int, CharResult>()
    solutionLower.forEachIndexed { index, solutionChar ->
        if (solutionChar == guessLower[index]) {
            resultMap[index] = CharResult(index, solutionChar, ResultType.CORRECT)
        }
    }
    guessLower.forEachIndexed { index, guessChar ->
        if (resultMap[index] == null) {
            if (solutionLower.contains(guessChar)) {
                if (resultMap.count { entry -> entry.value.char == guessChar } < solutionLower.count { c -> c == guessChar }) {
                    resultMap[index] = CharResult(index, guessChar, ResultType.PRESENT)
                } else {
                    resultMap[index] = CharResult(index, guessChar, ResultType.MISSING)
                }
            } else {
                resultMap[index] = CharResult(index, guessChar, ResultType.MISSING)
            }
        }
    }
    return resultMap
}

@Serializable
data class CharResult(
    val index: Int,
    val char: Char,
    val resultType: ResultType,
)

@Serializable
enum class ResultType {
    MISSING,
    CORRECT,
    PRESENT,
}
