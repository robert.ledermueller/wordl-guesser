package org.example.wordl.client.domain

import org.example.wordl.common.CharResult
import org.example.wordl.common.ResultType
import org.example.wordl.transfer.ComparisonResult
import org.junit.jupiter.api.Test
import kotlin.test.assertEquals

internal class MinimizingStrategyTest{

    @Test
    fun `only valid words left should not lead to an exception`() {
        val presentChars = mutableListOf('i', 'n', 't')
        val missingChars = mutableListOf('c', 'g', 'a', 'r', 'u', 's', 'y')
        val correctChars = mutableMapOf<Int,Char>()
        val usedGuesses = mutableListOf("nutsy", "cigar")
        val resultMap = mapOf(
            0 to CharResult(0, 'p', ResultType.CORRECT),
            1 to CharResult(1, 'h', ResultType.MISSING),
            2 to CharResult(2, 'l', ResultType.MISSING),
            3 to CharResult(3, 'o', ResultType.PRESENT),
            4 to CharResult(4, 'x', ResultType.MISSING)
        )
        val comparisonResult = ComparisonResult("unused", 1, resultMap)
        val sut = MinimizingStrategy(usedGuesses, missingChars, presentChars, correctChars)

        val nextGuess = sut.findNextGuess(comparisonResult)
        assertEquals("point", nextGuess)
    }

    @Test
    fun `minimized guesses are only used if they contain at least one of the most used chars`() {
        val presentChars = mutableListOf('a')
        val missingChars = mutableListOf('c', 'i', 'g', 'r', 'w', 'h', 'e', 'l', 'k')
        val correctChars = mutableMapOf<Int, Char>()
        val usedGuesses = mutableListOf("cigar", "whelk")
        val resultMap = mapOf(
            0 to CharResult(0, 'b', ResultType.MISSING),
            1 to CharResult(1, 'u', ResultType.MISSING),
            2 to CharResult(2, 's', ResultType.CORRECT),
            3 to CharResult(3, 't', ResultType.MISSING),
            4 to CharResult(4, 'y', ResultType.CORRECT)
        )
        val comparisonResult = ComparisonResult("unused", 1, resultMap)
        val sut = MinimizingStrategy(usedGuesses, missingChars, presentChars, correctChars)

        val nextGuess = sut.findNextGuess(comparisonResult)
        assertEquals("assay", nextGuess)
    }
}
