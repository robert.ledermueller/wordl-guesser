package org.example.wordl.client.domain

import org.example.wordl.client.transfer.WordlException
import org.example.wordl.client.transfer.WordlServerClient
import org.example.wordl.client.transfer.isSuccessfulGuess
import org.example.wordl.transfer.ComparisonResult

class WordGuesser(
    private val wordlServerClient: WordlServerClient,
    private val user: String,
    private val strategy: GuessingStrategy,
) {

    fun startGuessing() {
        println("===================================================================================")
        println("INFO: Trying to guess the secret word from wordl-server at ${wordlServerClient.wordlServerUrl}")
        println("===================================================================================\n")

        guess(1, strategy.findInitialGuess())
    }

    private fun guess(current: Int, guess: String) {

        println("$current try with guess: '$guess'")

        val comparisonResult: ComparisonResult
        try {
            comparisonResult = wordlServerClient.sendGuess(user, guess)
        } catch (e: WordlException) {
            println("===================================================================================")
            println("Game ended with message: ${e.message}")
            println("===================================================================================")
            return
        }

        // Success ?
        if (comparisonResult.isSuccessfulGuess()) {
            println("===================================================================================")
            println("INFO: Success! We found the solution: '$guess' after $current tries!")
            println("===================================================================================")
            return
        }

        // Determine next guess to use
        val nextGuess = strategy.findNextGuess(comparisonResult)

        println("INFO: Using '$nextGuess' as next guess")
        println("===================================================================================\n")
        guess(comparisonResult.currentTry + 1, nextGuess)
    }
}
