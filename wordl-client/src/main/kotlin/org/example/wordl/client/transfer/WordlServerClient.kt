package org.example.wordl.client.transfer

import io.ktor.client.*
import io.ktor.client.call.*
import io.ktor.client.request.*
import io.ktor.http.*
import kotlinx.coroutines.runBlocking
import org.example.wordl.transfer.ComparisonRequest
import org.example.wordl.transfer.ComparisonResult
import org.example.wordl.transfer.GameError

class WordlServerClient(
    private val httpClient: HttpClient,
    val wordlServerUrl: String,
) {

    fun sendGuess(user: String, guess: String): ComparisonResult {
        val comparisonResult: ComparisonResult
        runBlocking {
            val response = httpClient.post("$wordlServerUrl/guess") {
                contentType(ContentType.Application.Json)
                setBody(ComparisonRequest(user, guess))
            }
            if (!response.status.isSuccess()) {
                val gameError: GameError = response.body()
                throw WordlException(gameError.message)
            }
            comparisonResult = response.body()
        }
        return comparisonResult
    }

    fun resetGame(): Boolean {
        val success: Boolean
        runBlocking {
            val response = httpClient.post("$wordlServerUrl/reset")
            success = response.status.isSuccess()
        }
        return success
    }
}

class WordlException(message: String) : RuntimeException(message)
