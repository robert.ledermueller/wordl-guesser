package org.example.wordl.client.domain

import org.example.wordl.client.GUESSES
import org.example.wordl.client.SOLUTIONS
import org.example.wordl.client.getLines
import org.example.wordl.client.transfer.getCharResultsByType
import org.example.wordl.client.transfer.getGuessWord
import org.example.wordl.common.ResultType
import org.example.wordl.transfer.ComparisonResult

class SimpleGuessingStrategy(
    usedGuesses: MutableList<String> = mutableListOf(),
    missingChars: MutableList<Char> = mutableListOf(),
    presentChars: MutableList<Char> = mutableListOf(),
    correctChars: MutableMap<Int, Char> = mutableMapOf(),
) : GuessingStrategy(usedGuesses, missingChars, presentChars, correctChars) {

    override fun findNextGuess(comparisonResult: ComparisonResult): String {
        processCompareResult(comparisonResult)

        return filterSources(listOf(SOLUTIONS), getSolutionFilter())
            .also { println("INFO: Found ${it.size} possible guesses") }
            .random()
    }
}

class AnotherGuessingStrategy(
    usedGuesses: MutableList<String> = mutableListOf(),
    missingChars: MutableList<Char> = mutableListOf(),
    presentChars: MutableList<Char> = mutableListOf(),
    correctChars: MutableMap<Int, Char> = mutableMapOf(),
) : GuessingStrategy(usedGuesses, missingChars, presentChars, correctChars) {

    override fun findNextGuess(comparisonResult: ComparisonResult): String {
        processCompareResult(comparisonResult)

        val possibleNextGuesses = filterSources(listOf(SOLUTIONS), getSolutionFilter())
            .also { println("INFO: Found ${it.size} possible guesses") }

        if (possibleNextGuesses.size > 5) {
            val additionalGuesses = filterSources(listOf(SOLUTIONS), getMinimizingFilter())
                .also { println("INFO: Found ${it.size} possible guesses") }
            if (additionalGuesses.isNotEmpty()) {
                return selectNextGuess(additionalGuesses)
            }
        }
        return selectNextGuess(possibleNextGuesses)
    }

    /**
     * Select a word with the most different chars
     */
    private fun selectNextGuess(guesses: List<String>): String {
        return guesses
            .map { word -> word to word.toSet().count() }
            .sortedByDescending { it.second }
            .map { it.first }
            .first()
    }
}

class MinimizingStrategy(
    usedGuesses: MutableList<String> = mutableListOf(),
    missingChars: MutableList<Char> = mutableListOf(),
    presentChars: MutableList<Char> = mutableListOf(),
    correctChars: MutableMap<Int, Char> = mutableMapOf(),
) : GuessingStrategy(usedGuesses, missingChars, presentChars, correctChars) {

    override fun findNextGuess(comparisonResult: ComparisonResult): String {
        processCompareResult(comparisonResult)

        val possibleSolutions = filterSources(listOf(SOLUTIONS), getSolutionFilter())
        if (possibleSolutions.size == 1) {
            println("INFO: Only one option left")
            return possibleSolutions.first()
        }

        // Determine which chars filter the most words of the possible solutions
        val mostUsedChars = findCharsByWordCount(possibleSolutions)
        println("INFO: Most used chars: $mostUsedChars")

        // TODO: I have the feeling that filtering the results here isn't the best idea - try to verify with some tests!
        val minimizedGuess = filterSources(listOf(SOLUTIONS, GUESSES), getMinimizingFilter())

        // Extract best matches from minimized guesses
        // only if any word contains any of the most used chars!
        if (minimizedGuess.isNotEmpty() && containsAtLeastOnChar(minimizedGuess, mostUsedChars)) {
            println("INFO: Found minimized guesses: ${minimizedGuess.size}")
            return extractBestMatch(minimizedGuess, mostUsedChars)
        }

        // Fallback to best match from possible solutions
        println("INFO: Found next guesses: ${possibleSolutions.size}")
        return extractBestMatch(possibleSolutions, mostUsedChars)
    }
}

abstract class GuessingStrategy(
    private val usedGuesses: MutableList<String>,
    private val missingChars: MutableList<Char>,
    private val presentChars: MutableList<Char>,
    private val correctChars: MutableMap<Int, Char>,
) {
    abstract fun findNextGuess(comparisonResult: ComparisonResult): String

    /**
     * Find the word that uses 5 chars that are used the most in the solution list
     */
    fun findInitialGuess(): String {
        val mostUsedChars = findCharsByWordCount(getLines(SOLUTIONS).toList())
        return extractBestMatch(filterSources(listOf(SOLUTIONS, GUESSES), emptyList()), mostUsedChars)
    }

    internal fun processCompareResult(comparisonResult: ComparisonResult) {
        // Remember used guess words
        usedGuesses.add(comparisonResult.getGuessWord())

        // Remember correct chars and their position
        comparisonResult.getCharResultsByType(ResultType.CORRECT)
            .forEach { correctChars[it.index] = it.char }
        println("INFO: Current correct chars are: ${correctChars.toSortedMap()}")

        // Remember all present chars if they are not in the list of correct chars already
        presentChars += comparisonResult.getCharResultsByType(ResultType.PRESENT)
            .map { it.char }
            .filter { !correctChars.containsValue(it) && !presentChars.contains(it) }
        println("INFO: Current present chars are: $presentChars")

        // Remember all missing chars if they are neither known to be correct nor present
        missingChars += comparisonResult.getCharResultsByType(ResultType.MISSING)
            .map { it.char }
            .filter { !correctChars.containsValue(it) && !presentChars.contains(it) && !missingChars.contains(it) }
        println("INFO: Current missing chars are: $missingChars")
    }

    internal fun findCharsByWordCount(possibleGuesses: List<String>): List<Char> {
        val wordCharCount: MutableMap<Char, Int> = mutableMapOf()
        possibleGuesses.forEach { word ->
            word.toSet().forEach { wordCharCount.computeIfAbsent(it) { 0 }.plus(1) }
        }
        return wordCharCount.entries
            .sortedByDescending { it.value }
            .map { it.key }
    }

    internal fun containsAtLeastOnChar(words: List<String>, chars: List<Char>): Boolean {
        words.forEach { word -> word.forEach { char -> if (chars.contains(char)) return true } }
        return false
    }

    internal fun extractBestMatch(words: List<String>, chars: List<Char>): String {
        val result = mutableMapOf<String, Int>()

        words.forEach { word ->
            chars.forEachIndexed { index, char ->
                if (word.contains(char)) {
                    result.compute(word) { _, score -> score?.plus(chars.size - index) ?: (chars.size - index) }
                }
            }
        }
        return result.entries.maxBy { it.value }.key
    }

    internal fun filterSources(sources: List<String>, filter: List<(String) -> Boolean>): List<String> {
        return sources.flatMap { source ->
            getLines(source)
                .filter { word -> filter.all { it.invoke(word) } }
                .toList()
        }
    }

    internal fun getSolutionFilter(): List<(String) -> Boolean> {
        return listOf(
            { s -> containsNoChars(s, missingChars) },
            { s -> matchesCorrectChars(s, correctChars) },
            { s -> containsPresentChars(s, presentChars) },
            { s -> !usedGuesses.contains(s) }
        )
    }

    internal fun getMinimizingFilter(): List<(String) -> Boolean> {
        return listOf(
            { s -> containsNoChars(s, missingChars) },
            { s -> containsNoChars(s, presentChars) },
            { s -> containsNoChars(s, correctChars.values.toList()) },
        )
    }

    private fun containsNoChars(word: String, chars: List<Char>): Boolean {
        word.forEach { char -> if (chars.contains(char)) return false }
        return true
    }

    private fun containsPresentChars(word: String, presentChars: List<Char>): Boolean {
        presentChars.forEach { if (!word.contains(it)) return false }
        return true
    }

    private fun matchesCorrectChars(word: String, correctChars: Map<Int, Char>): Boolean {
        correctChars.forEach { (index, char) -> if (word[index] != char) return false }
        return true
    }
}
