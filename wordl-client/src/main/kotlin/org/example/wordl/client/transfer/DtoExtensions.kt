package org.example.wordl.client.transfer

import org.example.wordl.common.CharResult
import org.example.wordl.common.ResultType
import org.example.wordl.transfer.ComparisonResult

fun ComparisonResult.isSuccessfulGuess(): Boolean {
    results.forEach { (_, result) -> if (result.resultType != ResultType.CORRECT) return false }
    return true
}

fun ComparisonResult.getCharResultsByType(resultType: ResultType): List<CharResult> {
    return results
        .filter { (_, result) -> result.resultType == resultType }
        .map { it.value }
}

fun ComparisonResult.getGuessWord(): String {
    return results
        .toSortedMap()
        .map { it.value.char }
        .joinToString(separator = "")
}
