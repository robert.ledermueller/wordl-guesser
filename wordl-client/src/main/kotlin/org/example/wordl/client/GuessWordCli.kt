package org.example.wordl.client

import io.ktor.client.*
import io.ktor.client.engine.okhttp.*
import io.ktor.client.plugins.contentnegotiation.*
import io.ktor.serialization.kotlinx.json.*
import kotlinx.serialization.json.Json
import org.example.wordl.client.domain.AnotherGuessingStrategy
import org.example.wordl.client.domain.GuessingStrategy
import org.example.wordl.client.domain.MinimizingStrategy
import org.example.wordl.client.domain.SimpleGuessingStrategy
import org.example.wordl.client.domain.WordGuesser
import org.example.wordl.client.transfer.WordlServerClient
import java.util.stream.Stream
import kotlin.system.exitProcess

const val SOLUTIONS = "solutions.txt"
const val GUESSES = "guesses.txt"

fun main(args: Array<String>) {

    val strategy = getStrategy(args)
    val user = getUser(args)
    val server = getServer(args)
    val development = getDevelopmentMode(args)
    val httpClient = HttpClient(OkHttp) {
        install(ContentNegotiation) {
            json(Json {
                prettyPrint = true
                isLenient = true
                encodeDefaults = true
            })
        }
    }

    val wordlServerClient = WordlServerClient(httpClient, server)
    try {
        WordGuesser(wordlServerClient, user, strategy).startGuessing()
        if (development && wordlServerClient.resetGame()) {
            println("INFO: Development mode active: Game server was reset")
        }
    } catch (e: Exception) {
        println("===================================================================================")
        println("ERROR: During communication with the wordl-server: ${e.message}")
        println("===================================================================================")
    }
    httpClient.close()
}

fun getDevelopmentMode(args: Array<String>): Boolean {
    return args.contains("--development")
}

fun getServer(args: Array<String>): String {
    val serverIndex = args.indexOf("--server")
    if (serverIndex != -1) {
        val possibleServer = args.getOrNull(serverIndex + 1)
        if (possibleServer == null) {
            println("ERROR: --server is missing a valid argument like 'http://localhost:8080'")
            exitProcess(0)
        }
        return possibleServer
    }
    // Default
    return "http://localhost:8080"
}

fun getUser(args: Array<String>): String {
    val userIndex = args.indexOf("--user")
    if (userIndex != -1) {
        val possibleUser = args.getOrNull(userIndex + 1)
        if (possibleUser == null) {
            println("ERROR: --user is missing a valid argument like 'Anonymous'")
            exitProcess(0)
        }
        return possibleUser
    }
    // Default
    return "Anonymous"
}

fun getStrategy(args: Array<String>): GuessingStrategy {
    val strategyIndex = args.indexOf("--strategy")
    if (strategyIndex != -1) {
        val possibleStrategy = args.getOrNull(strategyIndex + 1)
        if (possibleStrategy == null) {
            println("ERROR: --strategy is missing a valid argument of ['simple', 'another']")
            exitProcess(0)
        }
        return when (possibleStrategy.lowercase()) {
            "simple" -> SimpleGuessingStrategy()
            "another" -> AnotherGuessingStrategy()
            "minimizing" -> MinimizingStrategy()
            else -> println("ERROR: '$possibleStrategy' is not a valid strategy argument, use on of ['simple', 'another', 'minimizing']")
                .run { exitProcess(0) }
        }
    }
    // Default
    return MinimizingStrategy()
}

fun getLines(source: String): Stream<String> {
    return object {}.javaClass.getResourceAsStream("/$source")?.bufferedReader()?.lines()
        ?: throw IllegalStateException("Cannot read $source file")
}
