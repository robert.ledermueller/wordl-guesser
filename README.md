# A simple version of the Wordl game with a server and client component #

To play the game you have to start the server first, then you can use the client to talk to the server.
The word used by the server can be provided via configuration or the server can choose a random word from the solutions list.

## How to use ##
The project should run with Java 8+ (only tested with Java 17) and uses Gradle (provided as executable with the project).

### wordl-server ###

To start the server run
```bash
./gradlew[.bat] :wordl-server:run
```

By default, the server will select a random word from the `solutions.txt`. You can specify a certain word by either
adding it in the `application.yaml` or by providing it as a cli parameter. Be aware that you still have to use a valid
word from the `solutions.txt`.

```yaml
wordl:
  game-word: valid-word
```

**Example**
```bash
./gradlew[.bat] :wordl-server:run --args="-P:wordl.game-word=fancy"
```

For development purposes the server provides a POST endpoint to reset the current game sessions and choose a new random word.
The endpoint is available at `/reset`. If you are using the default server configuration this is `http://localhost:8080/reset`

#### Server configuration ####

You can tweak certain aspects of the underlying server, like port, ssl port, threads, etc.
The configuration is done in the `application.yaml` file. An extensive documentation about the configuration options can
be found [ktor configuration documentation](https://ktor.io/docs/configurations.html#configuration-file)

### wordl-client ###

You can use the client to do an automated guessing against the wordl-server.

```bash
./gradlew[.bat] :wordl-client run
```

The wordl-client uses some default values which can be overwritten.

* --strategy [simple, another, minimizing] - defaults to 'minimizing'
* --user - defaults to 'anonymous'
* --server - defaults to 'http://localhost:8080'
* --development - development flag which makes sure that the server resets the word and game sessions after each run

**Example**
```bash
./gradlew[.bat] :wordl-client run --args="--development --strategy simple --user optimus --server http://localhost:8080"
```

## TODOs

* Implement extensive testing of strategies to verify how useful they are

## Maybes

* Implement a user frontend for the wordl-server
