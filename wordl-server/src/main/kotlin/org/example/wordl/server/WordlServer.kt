package org.example.wordl.server

import io.ktor.serialization.kotlinx.json.*
import io.ktor.server.application.*
import io.ktor.server.plugins.contentnegotiation.*
import kotlinx.serialization.Serializable
import kotlinx.serialization.json.Json
import org.example.wordl.server.plugins.configureRouting
import java.util.stream.Stream

const val SOLUTIONS = "solutions.txt"
const val GUESSES = "guesses.txt"

fun main(args: Array<String>): Unit = io.ktor.server.cio.EngineMain.main(args)

fun Application.module() {
    configureRouting()
    install(ContentNegotiation) {
        json(Json {
            prettyPrint = true
            isLenient = true
            encodeDefaults = true

        })
    }
    initWordlGame(environment.config.propertyOrNull("wordl.game-word")?.getString())
}

fun initWordlGame(gameWord: String?) {
    if (gameWord == null) {
        WordlGame.initializeWord(getLines(SOLUTIONS).toList().random())
    } else {
        val gameWordLower = gameWord.lowercase()
        if (getLines(SOLUTIONS).anyMatch { it == gameWord }) {
            WordlGame.initializeWord(gameWordLower)
            return
        }
        throw IllegalArgumentException("Initial word $gameWordLower is not a valid word")
    }
}

fun getLines(source: String): Stream<String> {
    return object {}.javaClass.getResourceAsStream("/$source")?.bufferedReader()?.lines()
        ?: throw IllegalStateException("Cannot read $source file")
}

object WordlGame {
    private val sessions: MutableMap<String, GameSession> = mutableMapOf()
    private var gameWord: String? = null

    /**
     * Initializes the game with given word and resets all [GameSession]s
     */
    fun initializeWord(word: String) {
        gameWord = word.lowercase()
        sessions.clear()
    }

    fun getWord(): String {
        return gameWord ?: throw IllegalStateException("Game wasn't initialized yet")
    }

    fun getTries(username: String): Int {
        return sessions.computeIfAbsent(username) { GameSession() }.tries
    }

    fun increaseTries(username: String): Int {
        return sessions.computeIfAbsent(username) { GameSession() }.increaseTries()
    }

    fun hasSolved(username: String): Boolean {
        return sessions.computeIfAbsent(username) { GameSession() }.solved
    }

    fun solve(username: String) {
        sessions.computeIfAbsent(username) {GameSession()}.solve()
    }

    fun getAllSession(): Map<String, GameSession> {
        return sessions.toMap()
    }
}

@Serializable
class GameSession {
    var solved: Boolean = false
        private set

    var tries: Int = 0
        private set

    fun increaseTries(): Int {
        return ++tries
    }

    fun solve() {
        solved = true
    }
}
