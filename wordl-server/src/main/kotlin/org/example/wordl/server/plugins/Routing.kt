package org.example.wordl.server.plugins

import io.ktor.http.*
import io.ktor.server.application.*
import io.ktor.server.request.*
import io.ktor.server.response.*
import io.ktor.server.routing.*
import org.example.wordl.server.WordlGame
import org.example.wordl.server.domain.compareWords
import org.example.wordl.server.initWordlGame
import org.example.wordl.transfer.ComparisonRequest
import org.example.wordl.transfer.GameError

fun Application.configureRouting() {

    routing {
        get("/") {
            call.respondText("Welcome to simple a Wordl server")
        }

        post("/guess") {
            val comparisonRequest = call.receive<ComparisonRequest>()
            try {
                call.respond(compareWords(comparisonRequest))
            } catch (e: Exception) {
                call.respond(
                    HttpStatusCode.BadRequest,
                    GameError(
                        comparisonRequest.username.lowercase(),
                        e.message ?: "Unknown error"
                    )
                )
            }
        }

        get("/users") {
            call.respond(WordlGame.getAllSession())
        }

        post("/reset") {
            initWordlGame(null)
            call.respond("Game was reset")
        }
    }
}
