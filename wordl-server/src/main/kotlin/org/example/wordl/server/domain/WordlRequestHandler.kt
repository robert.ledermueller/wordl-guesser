package org.example.wordl.server.domain

import org.example.wordl.common.ResultType
import org.example.wordl.common.compareWords
import org.example.wordl.server.GUESSES
import org.example.wordl.server.SOLUTIONS
import org.example.wordl.server.WordlGame
import org.example.wordl.server.getLines
import org.example.wordl.transfer.ComparisonRequest
import org.example.wordl.transfer.ComparisonResult

fun compareWords(comparisonRequest: ComparisonRequest): ComparisonResult {

    val username = comparisonRequest.username
    if (WordlGame.hasSolved(username)) {
        throw IllegalStateException("You already solved the word in ${WordlGame.getTries(username)} tries, come back tomorrow")
    }
    if (WordlGame.getTries(username) >= 6) {
        throw IllegalStateException("Maximum tries exhausted, come back tomorrow")
    }
    val guess = comparisonRequest.guess
    if (!isValidGuess(guess)) {
        throw IllegalArgumentException("$guess is not a valid word")
    }

    val resultMap = compareWords(WordlGame.getWord(), guess)
    if (resultMap.values.all { it.resultType == ResultType.CORRECT }) WordlGame.solve(username)
    return ComparisonResult(username, WordlGame.increaseTries(username), resultMap)
}

fun isValidGuess(guess: String): Boolean {
    listOf(SOLUTIONS, GUESSES).forEach { source ->
        if (getLines(source).anyMatch { it == guess }) return true
    }
    return false
}
